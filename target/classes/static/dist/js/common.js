(function($){$.getUrlParameter=
	function(name)
	{
		var reg=new RegExp("(^|&)"+name+"=([^&]*)(&|$)");
		var r=window.location.search.substr(1).match(reg);
		if (r!=null) return unescape(r[2]);
		return null;
	}
})(jQuery);

function _combotree(data, id) {
	var options = {
		bootstrap2 : false,
		showTags : true,
		levels : 5,
		checkedIcon : "glyphicon glyphicon-check",
		data : data,
		onNodeSelected : function(event, data) {
			$("#" + id).val(data.id);
			$("#" + id + "_view").val(data.text);
			$("#" + id + "_tree").hide();
		}
	};

	$("#" + id + "_tree").treeview(options);
	var curNode = $("#" + id + "_tree").treeview("getNode", $("#" + id).val());
	$("#" + id + "_view").val(curNode.text);
}
