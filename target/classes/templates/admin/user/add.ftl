<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times</span>
    </button>
    <h4 class="modal-title">创建用户</h4>
</div>
	  <div class="modal-body">
          <!-- 表单和内容 -->
          <form id="userAddForm">
              <div class="form-group clearfix">
                  <label id="usernameLabel" class="col-sm-2 control-label" style="text-align:right">用&nbsp;&nbsp;户&nbsp;&nbsp;名</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="username" id="username" placeholder="输入用户名...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label id="passwordLabel" class="col-sm-2 control-label" style="text-align:right">登录密码</label>
                  <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" id="password" placeholder="输入登录密码...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label id="emailLabel" class="col-sm-2 control-label" style="text-align:right">邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;箱</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="email" id="email" placeholder="输入邮箱...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label id="mobileLabel" class="col-sm-2 control-label" style="text-align:right">手&nbsp;&nbsp;机&nbsp;&nbsp;号</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="mobile" id="mobile" placeholder="输入手机号...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label id="sexLable" class="col-sm-2 control-label" style="text-align:right">性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别</label>
                  <div class="col-sm-10">
                      <input type="radio" checked="checked" name="sex" value="1">男
                      <input type="radio" name="sex" value="2">女
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label id="positionLabel" class="col-sm-2 control-label" style="text-align:right">位&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;置</label>
              <#--<div class="col-sm-10">-->
                  <div class="col-sm-3">
                  <#--<label id="provinceLable" class="col-sm-1 control-label">省</label>-->
                      <select class="form-control" id="province" name="province">
                          <option value="">请选择省</option>
                      </select>
                  </div>
                  <div class="col-sm-4">
                  <#--<label id="cityLable" class="col-sm-1 control-label">市</label>-->
                      <select class="form-control" id="city" name="city">
                          <option value="">请选择市</option>
                      </select>
                  </div>
                  <div class="col-sm-3">
                  <#--<label id="areaLable" class="col-sm-1 control-label">县</label>-->
                      <select class="form-control" id="district" name="district">
                          <option value="">请选择县（区）</option>
                      </select>
                  </div>
              <#--</div>-->
              </div>
              <div class="form-group clearfix">
                  <label id="roleLabel" class="col-sm-2 control-label" style="text-align:right">角&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;色</label>
                  <div class="col-sm-10">
					<#list roles as role>
                        <input name="role" type="checkbox" value="${role.id}">${role.roleName}&nbsp;&nbsp;&nbsp;
                    </#list>
                  </div>
              </div>
          </form>
      </div>
	  <div class="modal-footer">
          <!-- 按钮区域-->
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
          <button type="button" class="btn btn-primary" onclick="userSave();">保存</button>
      </div>
<script type="text/javascript">
    $(function () {
        // 初始化省市县，这里只需要初始化省
        var options = {
            url: '/admin/user/getProvince',
            type: 'get',
            dataType: 'json',
            success: function (data) {
                console.log(data);
                //遍历每条数据
                $.each(data, function (i, obj) {
                    var name = obj.province;
                    var id = obj.provinceId;
                    var $option = $("<option value='" + id + "'>" + name + "</option>");
                    //将option封装到s1中
                    $("#province").append($option);
                });
            }
        };
        $.ajax(options);

        //当省份域发生变化时触发
        $("#province").change(function () {
            //清空市域
            // $("#city").innerHTML = '';
            $("#city").empty();
            $("#city").append($("<option value=''>请选择市</option>"));
            //清空县域
            $("#district").empty();
            $("#district").append($("<option value=''>请选择县（区）</option>"));
            //获取已选省的value
            var $province_id = $(this).val();
            console.log($province_id);
            //向后台发送请求
            var options = {
                url: '/admin/user/getCity?provinceId=' + $province_id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    //遍历每条数据
                    $.each(data, function (i, obj) {
                        var name = obj.city;
                        var id = obj.cityId;
                        var $option = $("<option value='" + id + "'>" + name + "</option>");
                        //将option封装到s1中
                        $("#city").append($option);
                    });
                }
            };
            $.ajax(options);
        });
        //当市发生变化时触发
        $("#city").change(function () {
            //清空县域
            $("#district").empty();
            $("#district").append($("<option value=''>请选择县（区）</option>"));
            //获取已选省的value
            var $city_id = $(this).val();
            console.log($city_id);
            //向后台发送请求
            var options = {
                url: '/admin/user/getArea?cityId=' + $city_id,
                type: 'get',
                dataType: 'json',
                success: function (data) {
                    console.log(data);
                    //遍历每条数据
                    $.each(data, function (i, obj) {
                        var name = obj.area;
                        var id = obj.areaId;
                        var $option = $("<option value='" + id + "'>" + name + "</option>");
                        //将option封装到s1中
                        $("#district").append($option);
                    });
                }
            };
            $.ajax(options);
        });
    });

    function userSave() {
        //表单校验，不能为空
        $("span").remove(".errorClass");
        $("br").remove(".errorClass");
        var status = 1;
        var checked_val = "";
        if ($("#username").val() == "") {
            $("#usernameLabel").prepend('<span class="errorClass" style="color:red">*用户名不能为空</span><br class="errorClass"/>');
            status = 0;
        }
        if ($("#password").val() == "") {
            $("#passwordLabel").prepend('<span class="errorClass" style="color:red">*密码不能为空</span><br class="errorClass"/>');
            status = 0;
        }
        $("input:checkbox[name='role']:checked").each(function () {
            checked_val += $(this).val();
        });
        if (checked_val == "") {
            $("#roleLabel").prepend('<span class="errorClass" style="color:red">*请选择角色</span><br class="errorClass"/>');
            status = 0;
        }
        if (status == 0) {
            return false;
        } else {
            provinceName = $("#province").find("option:selected").text();
            cityName = $("#city").find("option:selected").text();
            areaName = $("#district").find("option:selected").text();
            var parm = $.param({
                "provinceName": provinceName,
                "cityName": cityName,
                "areaName": areaName
            }) + "&" + $("#userAddForm").serialize();
            console.log(parm);
            $.ajax({
                url: '/admin/user/save',
                type: 'post',
                dataType: 'json',
                data: parm,
                success: function (data) {
                    $("#lgModal").modal('hide');
                    alertMsg("添加成功", "success");
                    closeHandleLoading();
                    user_tab.draw(false);
                }
            });
        }

    }

    function changeCity() {
        alertMsg("用户名已经存在", "danger");
    }
</script>