package com.learn.hello.system.common.event;


import com.learn.hello.modules.entity.LoggerMessage;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//进程日志事件内容载体
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoggerEvent {
    private LoggerMessage log;
}
