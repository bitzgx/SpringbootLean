package com.learn.hello.system.common.conf;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.ehcache.EhCacheManager;
import org.apache.shiro.session.mgt.ExecutorServiceSessionValidationScheduler;
import org.apache.shiro.session.mgt.eis.EnterpriseCacheSessionDAO;
import org.apache.shiro.session.mgt.eis.JavaUuidSessionIdGenerator;
import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @ClassName ShiroConfiguration
 * @Deccription ShiroConfiguration配置
 * @Author DZ
 * @Date 2020/1/8 21:52
 **/
@Slf4j
@Configuration
public class ShiroConfiguration {
    /**
     * 设置shiro的缓存，缓存参数均配置在xml文件中，
     * 这个位置也可以使用redis缓存，但是shiro的缓存很少，没必要。
     */
    @Bean
    public EhCacheManager getEhCacheManager() {
        EhCacheManager em = new EhCacheManager();
        em.setCacheManagerConfigFile("classpath:ehcache/ehcache-shiro.xml");
        return em;
    }

    /**
     * 凭证匹配器
     * （由于我们的密码校验交给Shiro的SimpleAuthenticationInfo进行处理了
     * 所以我们需要修改下doGetAuthenticationInfo中的代码;
     * ）
     */
    /*@Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName("md5");//散列算法:这里使用MD5算法;
        hashedCredentialsMatcher.setHashIterations(1);//散列的次数，比如散列两次，相当于 md5(md5(""));
        return hashedCredentialsMatcher;
    }*/

    /**
     * 主文件
     */
    @Bean(name = "myShiroRealm")
    public MyShiroRealm myShiroRealm(EhCacheManager cacheManager) {
        MyShiroRealm realm = new MyShiroRealm();
        realm.setCacheManager(cacheManager);
//        realm.setCredentialsMatcher(hashedCredentialsMatcher());
        return realm;
    }

    //会话ID生成器
    @Bean(name = "sessionIdGenerator")
    public JavaUuidSessionIdGenerator javaUuidSessionIdGenerator() {
        JavaUuidSessionIdGenerator javaUuidSessionIdGenerator = new JavaUuidSessionIdGenerator();
        return javaUuidSessionIdGenerator;
    }

    /**
     * 配置保存sessionId的cookie
     * 注意：这里的cookie 不是上面的记住我 cookie 记住我需要一个cookie session管理 也需要自己的cookie
     */
    @Bean(name = "sessionIdCookie")
    public SimpleCookie getSessionIdCookie() {
        SimpleCookie sessionIdCookie = new SimpleCookie("sid");
        // 防止拥有HttpOnly的浏览器（IE6+，FF3.0+）进行XXS攻击
        sessionIdCookie.setHttpOnly(true);
        // maxAge=-1表示浏览器关闭时失效此Cookie
        sessionIdCookie.setMaxAge(-1);
        return sessionIdCookie;

    }

    //SessionDAO的作用是为Session提供CRUD并进行持久化的一个shiro组件
    //MemorySessionDAO 直接在内存中进行会话维护
    //EnterpriseCacheSessionDAO  提供了缓存功能的会话维护，默认情况下使用MapCache实现，内部使用ConcurrentHashMap保存缓存的会话。
    @Bean(name = "sessionDAO")
    public EnterpriseCacheSessionDAO enterpriseCacheSessionDAO() {
        EnterpriseCacheSessionDAO sessionDao = new EnterpriseCacheSessionDAO();
        sessionDao.setSessionIdGenerator(javaUuidSessionIdGenerator());
        // 设置session缓存的名字 默认为 shiro-activeSessionCache
        sessionDao.setActiveSessionsCacheName("shiro-activeSessionCache");
        return sessionDao;
    }

    @Bean(name = "sessionValidationScheduler")
    public ExecutorServiceSessionValidationScheduler getExecutorServiceSessionValidationScheduler() {
        ExecutorServiceSessionValidationScheduler scheduler = new ExecutorServiceSessionValidationScheduler();
        // 定时检查session是否过期
        scheduler.setInterval(1800000);
        return scheduler;
    }

    @Bean(name = "sessionManager")
    public DefaultWebSessionManager sessionManager(EnterpriseCacheSessionDAO sessionDAO) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        // 设置session过期时间
        sessionManager.setGlobalSessionTimeout(1800000);
        // 自动删除过期的session
        sessionManager.setDeleteInvalidSessions(true);
        // 开启回话调度器，这里主要用于检查回话是否过期
        sessionManager.setSessionValidationSchedulerEnabled(true);
        // 设置回话调度器（检查回话过期时间）
        sessionManager.setSessionValidationScheduler(getExecutorServiceSessionValidationScheduler());
        // 设置sessionDAO,用于对session的CRUD操作
        sessionManager.setSessionDAO(sessionDAO);
        // 启用自定义的SessionIdCookie
        sessionManager.setSessionIdCookieEnabled(true);
        sessionManager.setSessionIdCookie(getSessionIdCookie());
        //取消url 后面的 JSESSIONID
        sessionManager.setSessionIdUrlRewritingEnabled(false);
        return sessionManager;
    }

    @Bean(name = "lifecycleBeanPostProcessor")
    public LifecycleBeanPostProcessor getLifecycleBeanPostProcessor() {
        return new LifecycleBeanPostProcessor();
    }

    // 如果不加此注解，引入了spring aop的starter，会有一个奇怪的问题，导致shiro注解的请求，不能被映射
    @Bean
    public DefaultAdvisorAutoProxyCreator getDefaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator daap = new DefaultAdvisorAutoProxyCreator();
        // 在@Controller注解的类的方法中加入@RequiresRole等shiro注解，会导致该方法无法映射请求，导致返回404。
        // 让controller使用cglib代理而已，前缀感觉不太对
        daap.setProxyTargetClass(true);
        return daap;
    }

    @Bean(name = "securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(MyShiroRealm myShiroRealm, DefaultWebSessionManager sessionManager) {
        DefaultWebSecurityManager dwsm = new DefaultWebSecurityManager();
        //配置记住我 参考博客：
//        dwsm.setRememberMeManager(rememberMeManager());
//      <!-- 用户授权/认证信息Cache, 采用EhCache 缓存 -->
        dwsm.setCacheManager(getEhCacheManager());
        dwsm.setSessionManager(sessionManager);
        // 这个函数需要放到最后，否则授权无效（也是个奇怪的玩意）
        dwsm.setRealm(myShiroRealm);
        return dwsm;
    }

    /**
     * 开启shiro aop注解支持.
     * 可以在controller中的方法前加上注解， 如 @RequiresPermissions("userInfo:add")
     * 本项目中，所有的按钮也分配了权限，所以这里可以不增加这个bean
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor aasa = new AuthorizationAttributeSourceAdvisor();
        aasa.setSecurityManager(securityManager);
        return aasa;
    }


    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {
        // 这里有需要，也可以自定义shiro工厂，主要可以配置跟多的过滤条件与属性
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        // 必须设置 SecurityManager，作用：Web应用中,Shiro可控制的Web请求必须经过Shiro主过滤器的拦截
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/login");
        // 登录成功后要跳转的连接
        shiroFilterFactoryBean.setSuccessUrl("/certification");
        shiroFilterFactoryBean.setUnauthorizedUrl("/403");
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        // 配置拦截器
        // 1.配置不被拦截的过滤器=>anno
        /**************************************************************************************
         这里有个大坑：
         1.一定要按照顺序，否则位于此authc后面的的资源就算被放行依然会被拦截
         2.filterChainDefinitionMap必须为LinkedHashMap，用户保证顺序
         3.静态资源的目录不是static，不能直接filterChainDefinitionMap.put("/static/**", "anon");,这样写静态资源依然会被拦截，所以需要分别列举了所有的静态资源
         ***网上也有filterChainDefinitionMap.put("/static/**", "anon")生效的，好像需要单独配置过滤器
         **************************************************************************************/
        filterChainDefinitionMap.put("/login", "anon");
        filterChainDefinitionMap.put("/css/**", "anon");
        filterChainDefinitionMap.put("/dist/**", "anon");
        filterChainDefinitionMap.put("/dist_dd/**", "anon");
        filterChainDefinitionMap.put("/js/**", "anon");
        filterChainDefinitionMap.put("/plugins/**", "anon");
        // 2.配置退出过滤器，其中具体的退出代码shiro已经替我们实现了
        filterChainDefinitionMap.put("/logout", "logout");
        // 3.配置需要拦截的过滤器
        // 直接配置其它所有的url都被拦截=>authc---------------此过滤器一定要放在最后,否则位于此拦截器后面不拦截（anon）的资源依然会被拦截-----------
        filterChainDefinitionMap.put("/**", "authc");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        log.info("---------------shiro拦截器加载完成--------------------");
        return shiroFilterFactoryBean;
    }
}