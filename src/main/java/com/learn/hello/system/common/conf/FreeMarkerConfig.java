package com.learn.hello.system.common.conf;

import com.jagregory.shiro.freemarker.ShiroTags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class FreeMarkerConfig {

    @Autowired
    protected freemarker.template.Configuration configuration;

    @PostConstruct
    public void setSharedVariable() {

        // 页面对shiro标签的识别
        configuration.setSharedVariable("shiro", new ShiroTags());

    }

}
