package com.learn.hello.system.common.aspect;


import com.learn.hello.modules.entity.TLog;
import com.learn.hello.modules.entity.User;
import com.learn.hello.modules.service.LogService;
import com.learn.hello.system.common.annotation.Log;
import com.learn.hello.system.utils.HttpContextUtils;
import com.learn.hello.system.utils.IPUtils;
import com.learn.hello.system.utils.ShiroUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

@Slf4j
@Aspect
@Component
public class WebLogAspect {

    @Autowired
    private LogService logService;

    //两个..代表所有子目录，最后括号里的两个..代表所有参数
    @Pointcut("execution( * com.learn.hello.modules.controller.*.*(..))")
    public void logPointCut() {
    }

    //切点
    @Pointcut("@annotation(com.learn.hello.system.common.annotation.Log)")
    public void logPointCutLog() {
    }

    @Before("logPointCut()")
    public void doBefore(JoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Log syslog = method.getAnnotation(Log.class);
        // 记录下请求内容
        if (syslog != null) {
            log.info("操作 : {}", syslog.value());
        }
        log.info("请求地址 : {}", request.getRequestURL().toString());
        log.info("HTTP METHOD : {}", request.getMethod());
        log.info("CLASS_METHOD : {}.{}" + joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName());
        log.info("参数 : {}", Arrays.toString(joinPoint.getArgs()));

    }

    // returning的值和doAfterReturning的参数名一致
    @AfterReturning(returning = "ret", pointcut = "logPointCut()")
    public void doAfterReturning(Object ret) throws Throwable {
        // 处理完请求，返回内容(返回值太复杂时，打印的是物理存储空间的地址)
        log.debug("返回值 : {}", ret);
    }

    @Around("logPointCut()")
    public Object doAround(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object ob = pjp.proceed();// ob 为方法的返回值
        log.info("耗时 : {}", (System.currentTimeMillis() - startTime));
        return ob;
    }

    // 将日志存储到数据库
    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        long beginTime = System.currentTimeMillis();
        // 执行方法
        Object result = point.proceed();
        // 执行时长(毫秒)
        long time = System.currentTimeMillis() - beginTime;
        //异步保存日志
        saveLog(point, time);
        return result;
    }

    void saveLog(ProceedingJoinPoint joinPoint, long time) throws InterruptedException {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        TLog sysLog = new TLog();
        Log syslog = method.getAnnotation(Log.class);
        if (syslog != null) {
            // 注解上的描述
            sysLog.setOperation(syslog.value());
        }
        // 请求的方法名
        String className = joinPoint.getTarget().getClass().getName();
        String methodName = signature.getName();
        sysLog.setMethod(className + "." + methodName + "()");
        // 请求的参数
        Object[] args = joinPoint.getArgs();
        try {
            sysLog.setParams(Arrays.toString(args).substring(0, 4999));
        } catch (Exception e) {

        }
        // 设置IP地址
        // 获取request
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        // 设置IP地址
        sysLog.setIp(IPUtils.getIpAddr(request));
        // 用户名
        User currUser = ShiroUtils.getUser();
        if (null == currUser) {
            if (null != sysLog.getParams()) {
                sysLog.setUserId(-1);
                sysLog.setUsername(sysLog.getParams());
            } else {
                sysLog.setUserId(-1);
                sysLog.setUsername("获取用户信息为空");
            }
        } else {
            sysLog.setUserId(ShiroUtils.getUserId());
            sysLog.setUsername(ShiroUtils.getUser().getUsername());
        }
        sysLog.setTime((int) time);
        // 系统当前时间
        Date date = new Date();
        sysLog.setCreateTime(date);
        // 保存系统日志
//        logService.save(sysLog);
    }
}
