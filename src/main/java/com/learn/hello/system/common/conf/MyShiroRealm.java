package com.learn.hello.system.common.conf;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.entity.Role;
import com.learn.hello.modules.entity.User;
import com.learn.hello.modules.mapper.UserMapper;
import com.learn.hello.modules.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashSet;
import java.util.List;

/**
 * @ClassName MyShiroRealm
 * @Deccription authentication:认证，authorization：授权
 * @Author DZ
 * @Date 2020/1/8 21:30
 **/
public class MyShiroRealm extends AuthorizingRealm {


    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserService userService;


    /**
     * 获取授权信息,页面有shiro标签时，就会触动此方法
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        User user = (User) SecurityUtils.getSubject().getPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
        //获得该用户角色,并将权限加入到shiro中
        List<Role> roles = userMapper.getRoleByUsername(user.getUsername());
        HashSet<String> rolePermissions = new HashSet<String>();
        StringBuilder sb = new StringBuilder();
        for (Role role : roles) {
            rolePermissions.add(role.getRoleName());//这里约定一个属性即可
            sb.append(role.getId()).append(",");
        }
        authorizationInfo.setRoles(rolePermissions);
        //获取角色拥有的权限，力度更小（拥有上面的权限库，这个可以不要）
        if (null != roles && roles.size() > 0) {
            List<Menu> menus = userMapper.getMenusByRoleIds(sb.substring(0, sb.toString().length() - 1));
            HashSet<String> menuPermissions = new HashSet<String>();
            for (Menu menu : menus) {
                menuPermissions.add(menu.getPermission());
            }
            authorizationInfo.setStringPermissions(menuPermissions);
        }
        // 返回封装好的权限
        return authorizationInfo;
    }

    /**
     * 通过shiro中的realm获取应用程序中用户、角色和权限信息
     *
     * @param authenticationToken 用户信息token
     * @return 返回封装了用户信息的AuthenticationInfo实例
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        // 获取页面传入的用户名和密码
        String username = usernamePasswordToken.getUsername();
        String password = new String(usernamePasswordToken.getPassword());
        // 从数据库获取对应用户名密码的用户
        User user = userService.selectUserByName(username);
        if (null == user) {
            throw new UnknownAccountException("该账号不存在");
        } else if (!password.equals(user.getPassword())) {
            throw new IncorrectCredentialsException("密码不正确");
        }
        return new SimpleAuthenticationInfo(user, password, getName());
    }
}
