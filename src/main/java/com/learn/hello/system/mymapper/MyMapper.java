package com.learn.hello.system.mymapper;

import tk.mybatis.mapper.common.Mapper;

/**
 * @ClassName MyMapper
 * @Deccription 此接口不能被扫描到，否则会报错
 * @Author DZ
 * @Date 2019/12/26 21:53
 **/
public interface MyMapper<T> extends Mapper<T> {
}
