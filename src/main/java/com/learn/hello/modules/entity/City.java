package com.learn.hello.modules.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class City implements Serializable {
    private static final long serialVersionUID = -7839960723927095998L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "city_id")
    private String cityId;

    private String city;

    private String father;

}