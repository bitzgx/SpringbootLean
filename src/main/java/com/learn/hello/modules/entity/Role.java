package com.learn.hello.modules.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = "t_role")
public class Role implements Serializable {
    private static final long serialVersionUID = -4632998037417915107L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 角色名称
     */
    @Column(name = "role_name")
    private String roleName;

    /**
     * 角色描述
     */
    @Column(name = "role_remark")
    private String roleRemark;

    /**
     * 创建角色用户id
     */
    @Column(name = "user_id_create")
    private Integer userIdCreate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "update_time")
    private Date updateTime;

    private String usernameCreate;

}