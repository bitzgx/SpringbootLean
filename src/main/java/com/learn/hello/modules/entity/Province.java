package com.learn.hello.modules.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
public class Province implements Serializable {
    private static final long serialVersionUID = 1162008440165442339L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "province_id")
    private String provinceId;

    private String province;
}