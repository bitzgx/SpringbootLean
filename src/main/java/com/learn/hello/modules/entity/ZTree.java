package com.learn.hello.modules.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName ZTree
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/5 21:27
 **/
@Data
public class ZTree implements Serializable {
    private static final long serialVersionUID = 1524181997363001383L;
    private Integer id;
    private Integer pId;
    private String name;
    private Boolean open;
    private Boolean checked;

    // 页面获取pId的时候，通过如下方法获取的，大坑
    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }
}
