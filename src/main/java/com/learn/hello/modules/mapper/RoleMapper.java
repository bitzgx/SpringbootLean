package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.Role;
import com.learn.hello.system.mymapper.MyMapper;
import com.learn.hello.system.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;
import org.springframework.ui.ModelMap;

import java.util.List;

@CacheNamespace(implementation = RedisCache.class)
public interface RoleMapper extends MyMapper<Role> {
    List<Role> selectByCondition(ModelMap modelMap);

    Role selectById(int id);

    List<Role> selectAllRole();
}