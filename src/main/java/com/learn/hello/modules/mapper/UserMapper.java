package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.entity.Role;
import com.learn.hello.modules.entity.User;
import com.learn.hello.system.mymapper.MyMapper;
import com.learn.hello.system.utils.RedisCache;
import org.apache.ibatis.annotations.CacheNamespace;

import java.util.HashSet;
import java.util.List;

@CacheNamespace(implementation = RedisCache.class)
public interface UserMapper extends MyMapper<User> {
    String getPassword(String username);

    List<Role> getRoleByUsername(String username);

    List<Menu> getMenusByRoleIds(String roleIds);
}