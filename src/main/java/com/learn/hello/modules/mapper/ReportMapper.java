package com.learn.hello.modules.mapper;

import com.learn.hello.modules.entity.Report;
import com.learn.hello.system.mymapper.MyMapper;

public interface ReportMapper extends MyMapper<Report> {
    void insertTestData();
}