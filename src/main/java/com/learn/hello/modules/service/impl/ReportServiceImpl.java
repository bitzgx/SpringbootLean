package com.learn.hello.modules.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.hello.modules.entity.Report;
import com.learn.hello.modules.mapper.ReportMapper;
import com.learn.hello.modules.service.ReportService;
import com.learn.hello.system.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName ReportServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/20 20:28
 **/
@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ReportMapper reportMapper;

    @Override
    public PageUtil<Report> queryForPage(int pageCurrent, int pageSize) {
//        插入数据
        insertReport();
        PageHelper.startPage(pageCurrent, pageSize);
        PageInfo<Report> pageInfo = new PageInfo<Report>(reportMapper.selectAll(), pageSize);
        return new PageUtil<Report>(pageCurrent, pageSize, (int) pageInfo.getTotal(), (int) pageInfo.getTotal(), pageInfo.getList());
    }

    @Override
    public List<Report> getAllDate() {
        return reportMapper.selectAll();
    }

    private void insertReport() {
        for (int i = 0; i < 10000; i++) {
            reportMapper.insertTestData();
        }
    }
}
