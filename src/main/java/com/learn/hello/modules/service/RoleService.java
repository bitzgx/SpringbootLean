package com.learn.hello.modules.service;

import com.learn.hello.modules.entity.Role;
import com.learn.hello.modules.entity.ZTree;
import com.learn.hello.system.utils.PageUtil;

import java.util.List;

public interface RoleService {
    PageUtil<Role> page(int pageCurrent, int pageSize, String roleName, String roleRemark, String usernameCreate, String cols, String orderDir);

    Role selectById(int id);

    List<ZTree> getAllMenuByRoleId(String roleId);

    void roleUpdateSave(Role role);

    int deleteRole(String ids);

    void saveRole(Role role, String menuId);

    List<Role> selectAllRole();
}
