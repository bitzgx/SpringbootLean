package com.learn.hello.modules.service.impl;

import com.learn.hello.modules.entity.UserRole;
import com.learn.hello.modules.mapper.UserRoleMapper;
import com.learn.hello.modules.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName UserRoleServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/7 22:13
 **/
@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<Integer> getRoleId(String userId) {
        Example example = new Example(UserRole.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId", userId);
        List<UserRole> userRoles = userRoleMapper.selectByExample(example);
        List<Integer> list = new ArrayList<>();
        for (UserRole userRole : userRoles) {
            list.add(userRole.getRoleId());
        }
        return list;
    }

    @Override
    public void updateUserRoles(Integer userId, String roleIds) {
        // 根据userId删除老权限
        deleteRolesByUserId(userId);
        //增加最新的权限
        addRoles(userId, roleIds);
    }

    @Override
    public void save(Integer userId, String roleIds) {
        addRoles(userId, roleIds);
    }

    private void addRoles(Integer userId, String roleIds) {
        String[] roleId = roleIds.split(",");
        for (String role : roleId) {
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(Integer.parseInt(role));
            userRoleMapper.insertSelective(userRole);
        }
    }

    private void deleteRolesByUserId(Integer userId) {
        Example example = new Example(UserRole.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("userId", userId);
        userRoleMapper.deleteByExample(example);
    }
}
