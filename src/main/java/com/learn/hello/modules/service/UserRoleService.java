package com.learn.hello.modules.service;

import java.util.List;

public interface UserRoleService {
    List<Integer> getRoleId(String userId);

    void updateUserRoles(Integer userId, String roleIds);

    void save(Integer userId, String roleIds);
}
