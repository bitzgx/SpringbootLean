package com.learn.hello.modules.service.impl;

import com.learn.hello.modules.entity.RoleMenu;
import com.learn.hello.modules.mapper.RoleMenuMapper;
import com.learn.hello.modules.service.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;

/**
 * @ClassName RoleMenuServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/5 21:46
 **/
@Service
public class RoleMenuServiceImpl implements RoleMenuService {
    @Autowired
    private RoleMenuMapper roleMenuMapper;

    @Override
    public void roleUpdateSave(Integer id, String menuIds) {
        //1. 先删掉该角色所有的菜单权限
        Example example = new Example(RoleMenu.class);
        Example.Criteria criteria = example.createCriteria();
        criteria.andEqualTo("roleId", id);
        roleMenuMapper.deleteByExample(example);
        //2. 将新的权限增加进去
        String[] menuId = menuIds.split(",");
        for (String str : menuId) {
            RoleMenu roleMenu = new RoleMenu();
            roleMenu.setRoleId(id);
            roleMenu.setMenuId(Integer.parseInt(str));
            roleMenuMapper.insert(roleMenu);
        }
    }
}
