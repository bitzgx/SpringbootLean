package com.learn.hello.modules.service;

import com.learn.hello.modules.entity.TLog;
import com.learn.hello.system.utils.PageUtil;

/**
 * @ClassName LogService
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/12 15:02
 **/
public interface LogService {
    void save(TLog sysLog);

    PageUtil<TLog> listPage(int i, int pageSize);

    void delete(String id);
}
