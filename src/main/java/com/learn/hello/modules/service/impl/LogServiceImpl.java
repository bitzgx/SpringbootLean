package com.learn.hello.modules.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.learn.hello.modules.entity.TLog;
import com.learn.hello.modules.mapper.TLogMapper;
import com.learn.hello.modules.service.LogService;
import com.learn.hello.system.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName LogServiceImpl
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/12 15:02
 **/
@Slf4j
@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private TLogMapper tLogMapper;

    @Override
    public void save(TLog sysLog) {
        tLogMapper.insert(sysLog);
    }

    @Override
    public PageUtil<TLog> listPage(int pageCurrent, int pageSize) {
        PageHelper.startPage(pageCurrent, pageSize);
        List<TLog> tLogs = tLogMapper.selectAll();
        PageInfo<TLog> pageInfo = new PageInfo<>(tLogs, pageSize);
        PageUtil<TLog> resultData = new PageUtil<TLog>(pageCurrent, pageSize, (int) pageInfo.getTotal(), (int) pageInfo.getTotal(), pageInfo.getList());
        return resultData;
    }

    @Override
    public void delete(String id) {
        tLogMapper.deleteByPrimaryKey(id);
    }
}
