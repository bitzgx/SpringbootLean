package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.User;
import com.learn.hello.system.common.annotation.Log;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @ClassName LoginController
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/8 22:18
 **/
@Slf4j
@Controller
public class LoginController {

    @Log("登陆")
    @RequestMapping(value = "/")
    public String home(ModelMap map) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();
        if (user == null) {
            return "redirect:/login";
        } else {
            return "redirect:/index";
        }
    }

    /**
     * 进入登录页面
     */
    @Log("登陆")
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String getLogin(HttpSession session, ModelMap map) {
        // 读取缓存的数据
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();
        if (user == null) {
            return "login";
        } else {
            session.removeAttribute("msg");
            return "redirect:index";
        }
    }

    @Log("登陆")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String postLogin(HttpServletRequest request, String username, String password, ModelMap model, HttpSession session, RedirectAttributes redirectAttributes) {
        // 登录失败从request中获取shiro处理的异常信息。shiroLoginFailure:就是shiro异常类的全类名.
        UsernamePasswordToken usernamePasswordToken = new UsernamePasswordToken(username, password);
        Subject subject = SecurityUtils.getSubject();
        try {
            //登录操作
            subject.login(usernamePasswordToken);
            User user = (User) subject.getPrincipal();
            //更新用户登录时间，也可以在ShiroRealm里面做
            session.setAttribute("user", user);
            model.addAttribute("user", user);
            return "index";
        } catch (Exception e) {
            //登录失败从request中获取shiro处理的异常信息
            // shiroLoginFailure:就是shiro异常类的全类名，这里获取的为null，不知道什么原因。
            String exception = (String) request.getAttribute("shiroLoginFailure");
            // 这里，我通过自定义异常处理
            String message = e.getMessage();
            String msg = "";
            if (StringUtils.isNotBlank(message)) {
                if (message.equals("该账号不存在")) {
                    log.warn("UnknownAccountException -- > 账号不存在：");
                    msg = "用户不存在！";
                } else if (message.equals("密码不正确")) {
                    log.warn("IncorrectCredentialsException -- > 密码不正确：");
                    msg = "密码不正确！";
                } else if ("kaptchaValidateFailed".equals(exception)) {
                    // 目前还没有加验证码
                    log.warn("kaptchaValidateFailed -- > 验证码错误");
                    msg = "验证码错误!";
                } else {
                    msg = "用户名或密码不正确！";
                    log.warn("else -- >" + exception);
                }
            }
            //返回登录页面
            session.setAttribute("msg", msg);
            return "login";
        }
    }

    @RequestMapping(value = "/403", method = RequestMethod.GET)
    public String unauthorizedRole() {
        log.info("------没有权限-------");
        return "/error/403";
    }

    @Log("返回主页面")
    @RequestMapping(value = "/index")
    public String index(Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getPrincipal();
        if (user == null) {
            return "login";
        } else {
            model.addAttribute("user", user);
            return "index";
        }
    }
}

