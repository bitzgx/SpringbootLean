package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.*;
import com.learn.hello.modules.service.RoleService;
import com.learn.hello.modules.service.UserRoleService;
import com.learn.hello.modules.service.UserService;
import com.learn.hello.system.common.annotation.Log;
import com.learn.hello.system.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Slf4j
@Controller
@RequestMapping(value = "admin/user")
public class UserController {

    private static final String[] COLS_ORDER = {"", "", "username", "mobile", "email", "", "province", "city", "district", "", "update_time"};

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;

    @Log("返回用户列表")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public void list() {
    }

    @Log("查看用户")
    @ResponseBody
    @RequestMapping(value = "page", method = RequestMethod.POST)
    public PageUtil<User> page(@RequestParam(value = "start", defaultValue = "1") int start,
                               @RequestParam(value = "length", defaultValue = "10") int pageSize,
                               @RequestParam(value = "user_list_user_name", required = false) String userName,
                               @RequestParam(value = "user_list_true_phone", required = false) String mobile,
                               @RequestParam(value = "user_list_province", required = false) String province,
                               @RequestParam(value = "order[0][column]", required = false) Integer orderIndex,
                               @RequestParam(value = "order[0][dir]", required = false) String orderDir) {
        log.warn("查询用户列表");
        log.info("查询用户列表");
        return userService.page((start / pageSize) + 1, pageSize, userName, mobile, province, COLS_ORDER[orderIndex], orderDir);
    }

    @Log("删除用户")
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public int delete(@RequestParam(value = "checkVal") String[] ids) {
        int result = 0;
        result = userService.delete(ids);
        return result;
    }

    @Log("删除一个用户")
    @ResponseBody
    @RequestMapping(value = "/deleteOne", method = RequestMethod.GET)
    public void deleteOne(@RequestParam(value = "id") String id) {
        userService.deleteOne(id);
    }

    @Log("新增用户")
    @RequestMapping(value = "/add")
    public void addList(ModelMap modelMap) {
        //查询全部的角色
        List<Role> list = roleService.selectAllRole();
        modelMap.put("roles", list);
    }

    @Log("获取省")
    @ResponseBody
    @RequestMapping(value = "/getProvince", method = RequestMethod.GET)
    public List<Province> getProvince() {
        return userService.getProvince();
    }

    @Log("获取市")
    @ResponseBody
    @RequestMapping(value = "/getCity", method = RequestMethod.GET)
    public List<City> getCity(String provinceId) {
        return userService.getCityByParentId(provinceId);
    }

    @Log("获取区县")
    @ResponseBody
    @RequestMapping(value = "/getArea", method = RequestMethod.GET)
    public List<Area> getArea(String cityId) {
        return userService.getAreaByParentId(cityId);
    }

    @Log("保存用户")
    @ResponseBody
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(User user, @RequestParam(value = "role") String roleIds, String provinceName, String cityName, String areaName) {
        // 保存用户和角色
        user.setProvince(provinceName);
        user.setCity(cityName);
        user.setDistrict(areaName);
        userService.save(user);
        // 保存角色
        userRoleService.save(user.getId(), roleIds);
        return "1";
    }

    @Log("返回更新用户页面")
    @RequestMapping(value = "/update")
    public void getUserByUserid(@RequestParam(value = "id") String userId, ModelMap modelMap) {
        User user = userService.getUserByUserid(userId);
        modelMap.put("user", user);
        //根据UserId获取角色
        List<Integer> roleIds = userRoleService.getRoleId(userId);
        modelMap.put("userRoles", roleIds);
        List<Role> list = roleService.selectAllRole();
        modelMap.put("roles", list);
    }

    @Log("更新用户")
    @ResponseBody
    @RequestMapping(value = "/saveUpdate", method = RequestMethod.POST)
    public String saveUpdate(User user, @RequestParam(value = "role") String roleIds, String provinceName, String cityName, String areaName) {
        //更新用户表
        user.setProvince(provinceName);
        user.setCity(cityName);
        user.setDistrict(areaName);
        userService.updateUser(user);
        //更新权限表
        userRoleService.updateUserRoles(user.getId(), roleIds);

        return "1";
    }
}
