package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.UserOnline;
import com.learn.hello.modules.service.OnlineService;
import com.learn.hello.system.common.annotation.Log;
import com.learn.hello.system.utils.PageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName OnlineContraller
 * @Deccription 在线用户
 * @Author DZ
 * @Date 2020/1/12 10:41
 **/
@RequestMapping("/sys/online")
@Controller
public class OnlineContraller {
    @Autowired
    private OnlineService onlineService;

    @RequestMapping("/list")
    public void online() {
    }

    @Log("在线用户")
    @ResponseBody
    @RequestMapping(value = "/onlinePage", method = RequestMethod.POST)
    public PageUtil<UserOnline> queryForPage(@RequestParam(value = "start", defaultValue = "1") int start,
                                             @RequestParam(value = "length", defaultValue = "10") int pageSize) {
        PageUtil<UserOnline> result = onlineService.queryForPage((start / pageSize) + 1, pageSize);
        if (null != result) {
            return result;
        }
        return new PageUtil<UserOnline>();
    }

    @Log("强制用户下线")
    @ResponseBody
    @RequestMapping(value = "/forceLogout", method = RequestMethod.GET)
    public void forceLogout(@RequestParam(value = "id") String sessionId) {
        onlineService.forceLogout(sessionId);
    }
}