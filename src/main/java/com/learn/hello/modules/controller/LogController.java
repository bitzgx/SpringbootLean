package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.TLog;
import com.learn.hello.modules.service.LogService;
import com.learn.hello.system.common.annotation.Log;
import com.learn.hello.system.utils.PageUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName LogController
 * @Deccription TODO
 * @Author DZ
 * @Date 2020/1/12 14:44
 **/
@RequestMapping(value = "/sys/log")
@Controller
@Slf4j
public class LogController {
    @Autowired
    private LogService logService;

    @Log("查看日志列表List")
    @RequestMapping(value = "/list")
    public void list() {
    }

    @Log("查看日志列表listPage")
    @ResponseBody
    @RequestMapping(value = "/listPage", method = RequestMethod.POST)
    public PageUtil<TLog> listPage(@RequestParam(value = "start", defaultValue = "1") int start,
                                   @RequestParam(value = "length", defaultValue = "10") int pageSize) {
        PageUtil<TLog> result = logService.listPage((start / pageSize) + 1, pageSize);
        if (result == null) {
            return new PageUtil<TLog>();
        }
        return result;
    }

    @Log("删除日志列表")
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public void delete(@RequestParam(value = "id") String id) {
        logService.delete(id);
    }
}
