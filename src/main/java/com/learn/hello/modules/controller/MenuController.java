package com.learn.hello.modules.controller;

import com.learn.hello.modules.entity.Menu;
import com.learn.hello.modules.service.MenuService;
import com.learn.hello.system.common.annotation.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName MenuController
 * @Deccription 菜单管理
 * @Author DZ
 * @Date 2020/1/5 10:41
 **/
@RequestMapping(value = "admin/menu")
@Controller
@Slf4j
public class MenuController {

    @Autowired
    private MenuService menuService;

    @Log("返回list页面")
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public void list() {
    }

    @Log("查看菜单")
    @ResponseBody
    @RequestMapping(value = "page")
    public List<Menu> page() {
        List<Menu> listAll = new ArrayList<>();
        //1.查询根节点,根据序列号查询，序列号为0的表示为根节点（约定）
        List<Menu> listParent = menuService.getMenuByParentId(0);
        listAll.addAll(listParent);
        //2.初始化第二层（这个也可以不初始化，通过点击扩展展开），这里初始化，界面更加友好
        List<Menu> listSecond = menuService.getSecondMenuByParentId(listParent);
        listAll.addAll(listSecond);
        //3.初始化是否具有子节点
        initHasChild(listAll);
        return listAll;
    }

    private void initHasChild(List<Menu> list) {
        for (Menu t : list) {
            int childNodNum = menuService.getchildNodNumByParentId(t.getId());
            if (childNodNum > 0) {
                t.setHasChild(true);
            } else {
                t.setHasChild(false);
            }
        }
    }

    @Log("扩展菜单")
    @ResponseBody
    @RequestMapping(value = "expand")
    public List<Menu> expand(@RequestParam(value = "parentId") int parentId) {
        List<Menu> list = menuService.getMenuByParentId(parentId);
        initHasChild(list);
        return list;
    }

    @Log("返回菜单")
    @ResponseBody
    @RequestMapping(value = "delete")
    public void delete(int id) {
        menuService.deleteById(id);
    }

    @RequestMapping(value = "add")
    public void add(int id, ModelMap modelMap) {
        Menu menu = menuService.getMenuById(id);
        modelMap.put("menu", menu);
    }

    @Log("增加菜单")
    @ResponseBody
    @RequestMapping(value = "save", method = RequestMethod.GET)
    public String save(Menu menu) {
        menuService.save(menu);
        return "0";
    }

    @Log("返回更新页面")
    @RequestMapping(value = "update")
    public void update(int id, ModelMap modelMap) {
        Menu menu = menuService.getMenuById(id);
        modelMap.put("menu", menu);
    }

    @Log("更新菜单")
    @ResponseBody
    @RequestMapping(value = "saveUpdate", method = RequestMethod.POST)
    public String saveUpdate(Menu menu) {
        menuService.saveUpdate(menu);
        return "0";
    }

}
