<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times</span>
    </button>
    <h4 class="modal-title">创建菜单</h4>
</div>
	  <div class="modal-body">
          <!-- 表单和内容 -->
          <form id="menuAddForm">
              <input type="hidden" id="parentId" name="parentId" value="${menu.id!}">
              <div class="form-group clearfix">
                  <label class="col-sm-2 control-label" style="text-align:right">父节点</label>
                  <div class="col-sm-10">
                      <input type="text" readonly="true" class="form-control" name="parentName" id="parentName" value="${menu.name!}">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label class="col-sm-2 control-label" id="nameLable" style="text-align:right">菜单名称</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="name" id="name" placeholder="输入菜单名称...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label class="col-sm-2 control-label" id="typeLable" style="text-align:right">菜单类型</label>
                  <div class="col-sm-10">
                      <select class="form-control" id="type" name="type">
                          <option value="">请选择</option>
                          <option value="1">目录</option>
                          <option value="2">菜单</option>
                          <option value="3">按钮</option>
                      </select>
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label class="col-sm-2 control-label" style="text-align:right">访问地址</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="url" id="url" placeholder="输入访问地址...">
                  </div>
              </div>
              <div class="form-group clearfix">
                  <label class="col-sm-2 control-label" id="permissionLable" style="text-align:right">权限名称</label>
                  <div class="col-sm-10">
                      <input type="text" class="form-control" name="permission" id="permission" placeholder="输入权限名称...">
                  </div>
              </div>
          </form>
      </div>
	  <div class="modal-footer">
          <!-- 按钮区域-->
          <button type="button" class="btn btn-primary" onclick="menuSave();">保存</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
      </div>
<script type="text/javascript">
    function menuSave() {
        //表单校验，不能为空
        $("span").remove(".errorClass");
        $("br").remove(".errorClass");
        var status = 1;
        var checked_val = "";
        if ($("#name").val() == "") {
            $("#nameLable").prepend('<span class="errorClass" style="color:red">*菜单名不能为空</span><br class="errorClass"/>');
            status = 0;
        }
        if ($("#type").val() == "") {
            $("#typeLable").prepend('<span class="errorClass" style="color:red">*菜单类型不能为空</span><br class="errorClass"/>');
            status = 0;
        }
        if ($("#permission").val() == "") {
            $("#permissionLable").prepend('<span class="errorClass" style="color:red">*权限名称不能为空</span><br class="errorClass"/>');
            status = 0;
        }
        if (status == 0) {
            return false;
        } else {
            $.ajax({
                url: '/admin/menu/save',
                type: 'get',
                dataType: 'json',
                data: $("#menuAddForm").serialize(),
                success: function (data) {
                    console.log(data);
                    $("#lgModal").modal('hide');
                    alertMsg("添加成功", "success");
                    closeHandleLoading();
                    loadMenuTable();
                }
            });
        }

    }
</script>