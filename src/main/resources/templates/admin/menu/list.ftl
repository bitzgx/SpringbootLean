<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>用户列表</title>
    <link href="/plugins/treeTable/themes/vsStyle/treeTable.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="box-body">
    <table id="menu_tab" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>名称</th>
            <th>类型</th>
            <th>连接</th>
            <th>权限</th>
            <th>更新用户</th>
            <th>更新时间</th>
            <th>是否可用</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
</div>
<script src="/plugins/treeTable/jquery.treeTable.min.js" type="text/javascript"></script>
<script>
    function loadMenuTable() {
        $.ajax({
            url: '/admin/menu/page',
            type: 'post',
            dataType: 'json',
            success: function (data) {
                if (data != null) {
                    var html = "";
                    for (var int = 0; int < data.length; int++) {
                        var item = data[int];
                        if (item.hasChild == true) {
                            html += "<tr id='" + item.id + "' pId='" + item.parentId + "' haschild='true'>";
                        } else {
                            html += "<tr id='" + item.id + "' pId='" + item.parentId + "')>";
                        }

                        html +=
                                "<td nowrap>" + item.name + "</td>" +
                                "<td nowrap>" + getLableType(item.type) + "</td>" +
                                "<td nowrap>" + item.url + "</td>" +
                                "<td nowrap>" + item.permission + "</td>" +
                                "<td nowrap>" + item.username + "</td>" +
                                "<td nowrap>" + item.updateDate + "</td>" +
                                "<td nowrap>" + getFlag(item.delFlag) + "</td>" +
                                "<td nowrap>" + oprate(item.id) + "</td>" +
                                "</tr>";
                    }
                    $("#menu_tab tbody").html(html);
                    initMenuTreeTable();
                }
            }
        });
    }

    function initMenuTreeTable() {
        var option = {
            // theme: 'vsStyle',
            expandLevel: 2,
            beforeExpand: function ($treeTable, id) {
                // alert(id);
                //判断id是否已经有了孩子节点，如果有了就不再加载，这样就可以起到缓存的作用
                if ($('.' + id, $treeTable).length) {
                    return;
                }
                //这里的html可以是ajax请求
                openHandleLoading();
                var childHtml = "";
                // alert('childHtml' + id);
                $.ajax({
                    url: '/admin/menu/expand?parentId=' + id,
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        // alert(data);
                        if (data != null) {
                            //var html = "";
                            for (var int = 0; int < data.length; int++) {
                                var item = data[int];
                                if (item.hasChild == true) {
                                    childHtml += "<tr id='" + item.id + "' pId='" + item.parentId + "' haschild='true' >";
                                } else {
                                    childHtml += "<tr id='" + item.id + "' pId='" + item.parentId + "'>";

                                }
                                childHtml +=
                                        "<td nowrap>" + item.name + "</td>" +
                                        "<td nowrap>" + getLableType(item.type) + "</td>" +
                                        "<td nowrap>" + item.url + "</td>" +
                                        "<td nowrap>" + item.permission + "</td>" +
                                        "<td nowrap>" + item.username + "</td>" +
                                        "<td nowrap>" + item.updateDate + "</td>" +
                                        "<td nowrap>" + getFlag(item.delFlag) + "</td>" +
                                        "<td nowrap>" + oprate(item.id) + "</td>" +
                                        "</tr>";
                            }

                        }
                        $treeTable.addChilds(childHtml);
                        closeHandleLoading();
                    }
                });
            },
            onSelect: function ($treeTable, id) {
                //console.log('onSelect:' + id);
            }

        };

        $("#menu_tab").treeTable(option);
        // $("#menu_tab").treeTable(option).show();
        //$("#menu_tab").treeTable({expandLevel : 2,column:0}).show();
    }

    $(function () {
        loadMenuTable();
    });

    // 修改按钮的样式
    function getLableType(type) {
        var typeName = "";
        if (type === 1) {
            typeName = "<span class='btn btn-xs btn-primary'>目录</span>";
        } else if (type === 2) {
            typeName = "<span class='btn btn-xs btn-success'>菜单</span>";
        } else {
            typeName = "<span class='btn btn-xs btn-info'>按钮</span>";
        }
        return typeName;
    }

    // 是否可用
    function getFlag(flag) {
        var flagName = "";
        if (flag === 2) {
            flagName = "是";
        } else {
            flagName = "<font color='red'>否</font>"
        }
        return flagName;
    }

    // 返回操作
    function oprate(id) {
        var add = '<a class="btn btn-xs btn-info"  target="modal" modal="lg" href="/admin/menu/add?id=' + id + '"><i class="fa fa-edit"></i>增加</a> &nbsp;';
        var update = '<a class="btn btn-xs btn-warning"  target="modal" modal="lg" href="/admin/menu/update?id=' + id + '"><i class="fa fa-edit"></i>修改</a> &nbsp;';
        var delet = '<a class="btn btn-xs btn-danger"     onclick="menu_list_delete(' + id + ')"><i class="fa fa-remove"></i>删除</a> &nbsp;';
        return add + update + delet;
    }

    //删除
    function menu_list_delete(param) {
        var href = "/";
        var title = "<p>警告！ 是否确认删除！</p>";
        var cb = "menu_list_delete_one_data('" + param + "');";
        $("#smModal").attr("action", href).attr("callback", cb).find(".modal-body").html(title).end().modal("show");
    }

    //具体删除逻辑
    function menu_list_delete_one_data(id) {
        var options = {
            url: '/admin/menu/delete?id=' + id,
            type: 'get',
            dataType: 'text',
            success: function (data) {
                loadMenuTable();
                alertMsg("<p>删除成功</p>", "success");
            }
        };
        $.ajax(options);
    }

</script>
</body>
