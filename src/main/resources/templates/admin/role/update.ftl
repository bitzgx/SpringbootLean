<#--<link rel="stylesheet" href="/plugins/ztree/css/demo.css" type="text/css">-->
<link rel="stylesheet" href="/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times</span>
    </button>
    <h4 class="modal-title">更新角色</h4>
</div>
<div class="modal-body">
    <!-- 表单和内容 -->
    <form id="roleUpdateForm">
        <input type="hidden" class="form-control" name="id" id="id" value="${role.id}">
        <input type="hidden" class="form-control" name="userIdCreate" id="userIdCreate" value="${role.userIdCreate}">
        <input type="hidden" class="form-control" name="createTimeName" id="createTimeId" value="${role.createTime?string("yyyy-MM-dd hh:mm:ss")}">
        <input type="hidden" class="form-control" name="menuId" id="menuId">
        <div class="form-group clearfix">
            <label id="roleNameLabel" class="col-sm-2 control-label" style="text-align:right">角色名称</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="roleName" id="roleName" value="${role.roleName}">
            </div>
        </div>
        <div class="form-group clearfix">
            <label id="roleRemarkLabel" class="col-sm-2 control-label" style="text-align:right">角色描述</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="roleRemark" id="roleRemark" value="${role.roleRemark}">
            </div>
        </div>
        <div class="form-group clearfix">
            <label id="menuRoleId" class="col-sm-2 control-label" style="text-align:right">菜单权限</label>
            <div class="col-sm-10">
                <div id="treeDemoUpdate" class="ztree"></div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <!-- 按钮区域-->
    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
    <button type="button" class="btn btn-primary" onclick="roleUpdateSave();">保存</button>
</div>

<#--<script type="text/javascript" src="/plugins/ztree/js/jquery-1.4.4.min.js"></script>-->
<script type="text/javascript" src="/plugins/ztree/js/jquery.ztree.core.js"></script>
<script type="text/javascript" src="/plugins/ztree/js/jquery.ztree.excheck.js"></script>
<SCRIPT type="text/javascript">
    var setting = {
        check: {
            enable: true
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            beforeCheck: true,
            onCheck: zTreeOnClick
        }
    };
    var zNodes;
    var code;

    function setCheck() {
        var zTree = $.fn.zTree.getZTreeObj("treeDemoUpdate"),
                type = {"Y": "ps", "N": "ps"};
        zTree.setting.check.chkboxType = type;
        showCode('setting.check.chkboxType = { "Y" : "' + type.Y + '", "N" : "' + type.N + '" };');
    }

    function showCode(str) {
        if (!code) code = $("#code");
        code.empty();
        code.append("<li>" + str + "</li>");
    }

    $(document).ready(function () {
        $.ajax({
            url: '/admin/role/roleUpdateTree?roleId=' +${role.id},
            type: 'post',
            async: false,
            dataType: 'json',
            success: function (data) {
                zNodes = data;
            }
        });
        $.fn.zTree.init($("#treeDemoUpdate"), setting, zNodes);
        setCheck();
    });

    function zTreeOnClick(event, treeId, treeNode) {
        var treeObj = $.fn.zTree.getZTreeObj("treeDemoUpdate");
        var nodes = treeObj.getCheckedNodes(true);
        var ids = "";
        for (var i = 0; i < nodes.length; i++) {
            ids += nodes[i].id + ",";
        }
        $("#menuId").val(ids.substring(0, ids.length - 1));
    }

    function roleUpdateSave() {

        $.ajax({
            url: '/admin/role/roleUpdateSave',
            type: 'post',
            data: $("#roleUpdateForm").serialize(),
            success: function (data) {
                if (data == "1") {
                    alertMsg("修改失败，存在为空", "warning");
                } else {
                    $("#lgModal").modal('hide');
                    alertMsg("修改成功", "success");
                    closeHandleLoading();
                    role_tab.draw(false);
                }

            }
        });
    }
</script>