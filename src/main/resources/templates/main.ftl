<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>欢迎页</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <script src="js/websocket/sockjs.min.js"></script>
    <script src="js/websocket/stomp.min.js"></script>
</head>
<body>
<div class="panel panel-default">
    <h1>jvm进程内的日志</h1>
    <button onclick="openSocket()">开启日志</button>
    <button onclick="closeSocket()">关闭日志</button>
    <div id="log-container" style="height: 600px; overflow-y: scroll; background: #333; color: #aaa; padding: 10px;">
        <div></div>
    </div>
</div>
<script>
    var stompClient = null;
    $(document).ready(function () {
        // openSocket();
    });

    function openSocket() {
        if (stompClient == null) {
            var socket = new SockJS('http://localhost:8080/websocket?token=kl');
            stompClient = Stomp.over(socket);
            stompClient.connect({token: "kl"}, function (frame) {
                stompClient.subscribe('/topic/pullLogger', function (event) {
                    var content = JSON.parse(event.body);
                    $("#log-container div").append("<font color='red'>" + content.timestamp + "</font>|<font color='highlight'>" + content.level + "</font> |<font color='green'>" + content.threadName + "</font>| <font color='boldMagenta'>" + content.className + "</font>|<font color='cyan'>" + content.body + "</font>").append("<br/>");
                    $("#log-container").scrollTop($("#log-container div").height() - $("#log-container").height());
                }, {
                    token: "kltoen"
                });
            });
        }
    }

    function closeSocket() {
        if (stompClient != null) {
            stompClient.disconnect();
            stompClient = null;
        }
    }
</script>
</body>
</html>