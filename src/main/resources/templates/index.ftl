<!DOCTYPE html>
<html>
<head>
<#include "/macro/base.ftl" />
<!-- <base href="/static/"> -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>SpringBootEdu</title>
<@style/>
</head>

<style>
    th,td{
        white-space:nowrap;
    }
</style>

<body class="sidebar-mini ajax-template skin-blue fixed">
	<div class="wrapper">
		<@header/>
		<@menu/>
		<div class="content-wrapper">
			<section class="content-header-navtabs">
				<div class="tabs-page">
					<ul class="tabs-list clearfix" id="navTabs">
						<li class="active" id="tab_sysindex">
							<span>我的主页</span>
						</li>
					</ul>
					<a href="javascript:void(0);" class="prev fa fa-step-backward"></a>
					<a href="javascript:void(0);" class="next fa fa-step-forward"></a>
				</div>
				<div class="context-menu" id="contextMenu">
					<ul class="ct-nav">
						<li rel="reload">刷新标签页</li>
						<li rel="closeCurrent">关闭标签页</li>
						<li rel="closeOther">关闭其他标签页</li>
						<li rel="closeAll">关闭全部标签页</li>
					</ul>
				</div>
			</section>
			<section class="content" id="content">
				<div class="tabs-panel" style="width:100%;height: 100%;">
				<p class="pull-right">
				</p>
				     <#-- <iframe name="mainFrame" id="mainFrame" frameborder="0" src="/geoServerDemo" style="margin:0 auto;width:100%;height: 100%;" scrolling="yes">-->
				      <iframe name="mainFrame" id="mainFrame" frameborder="0" src="/main" style="margin:0 auto;width:100%;height: 100%;" scrolling="yes">

					  </iframe>
				</div>
			</section>
		</div>
		<@setting/>
	</div>
	<@jsFile/>
</body>
<script type="text/javascript">
function fullScreen(){
	
	//$("#mainFrame").height(1050);
	$('.main-sidebar').hide();
	$(".content-wrapper").css("margin-left","0px");
	$('.main-header').hide();
	$('.content-header-navtabs').hide();
	$(".content-wrapper").css("padding-top","0px");
	$(".content").css("margin-top","0px");
	$(".content").css("padding","0px");
	$(".content").css("padding-left","0px");
	$(".content").css("padding-right","0px");
	/* $("#mainFrame").css("height","780px"); */
	//$("#mainFrame1").attr("scrolling","yes");
	
	
	
	
}
function outfullScreen(){
	
	$('.main-sidebar').show();
	$(".content-wrapper").css("margin-left","230px");
	$('.main-header').show();
	$('.content-header-navtabs').show();
	$(".content-wrapper").css("padding-top","50px");
	$(".content").css("margin-top","40px");
	$(".content").css("padding","7px");
	$(".content").css("padding-left","7px");
	$(".content").css("padding-right","7px");
	if($("#mainFrame").height()<630)
		$("#mainFrame").css("height","680px");
	else
		$("#mainFrame").css("height","100%");
	//$("#mainFrame1").attr("scrolling","no");
	
}

   
</script>
</html>

