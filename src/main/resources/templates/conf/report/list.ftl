<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>报表导出</title>
</head>
<body>
<div class="box-body">
    <div class="clearfix">
        <form class="form-horizontal">
            <div class="form-group clearfix">
                <button type="button" onclick="report_poi();" class="btn btn-sm btn-warning">poi导出</button>
                <button type="button" onclick="report_jxl();" class="btn btn-sm btn-danger">jxl导出</button>
                <button type="button" onclick="report_esay_excel();" class="btn btn-sm btn-primary">esayExcel导出</button>
            </div>
        </form>
    </div>
    <table id="report_tab" class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>id</th>
            <th>报表id</th>
            <th>col1</th>
            <th>col2</th>
            <th>col3</th>
            <th>col4</th>
            <th>col5</th>
            <th>col11</th>
            <th>col12</th>
            <th>col13</th>
            <th>col14</th>
            <th>col15</th>
            <th>col21</th>
            <th>col22</th>
            <th>col23</th>
            <th>col24</th>
            <th>col25</th>
        </tr>
        </thead>
    </table>
</div>
<script type="text/javascript">
    var report_tab;
    $(function () {
        var url = "/conf/report/listPage";
        report_tab = $('#report_tab').DataTable({
            "fnDrawCallback": function () {
            },
            "dom": '<"top"i>rt<"bottom"flp><"clear">',
            "processing": true,
            "searching": false,
            "serverSide": true,   //启用服务器端分页
            // "order": [[ 8, "asc" ]],//默认排序字段
            "bInfo": true,
            "bAutoWidth": false,
            "scrollX": true,
            "scrollCollapse": false,
            "language": {"url": "/plugins/datatables/language.json"},
            "ajax": {"url": url, "type": "post"},
            "columns": [
                {"data": "id"},
                {"data": "reportId"},
                {"data": "col1"},
                {"data": "col2"},
                {"data": "col3"},
                {"data": "col4"},
                {"data": "col5"},
                {"data": "col11"},
                {"data": "col12"},
                {"data": "col13"},
                {"data": "col14"},
                {"data": "col15"},
                {"data": "col21"},
                {"data": "col22"},
                {"data": "col23"},
                {"data": "col24"},
                {"data": "col25"}
            ]
        }).on('preXhr.dt', function (e, settings, data) {
            No = 0;
        }).on('xhr.dt', function (e, settings, json, xhr) {
        });
    });

    function report_poi() {
        window.location.href = "/conf/report/reportPoi";
    }

    function report_jxl() {
        window.location.href = "/conf/report/reportJxl";
    }

    function report_esay_excel() {
        window.location.href = "/conf/report/reportEsayExcel";
    }
</script>
</body>
</html>