;(function ($) {
    $.fn.anydrag = function (options) {
        var options = $.extend({
            hotregion: ""//热区
        }, options);//定义参数
        var handle = $(this);
        var mousedown = false;
        var offsetX = 0;
        var offsetY = 0;
        handle.each(function () {
            var target = $(this);
            var height = handle.height();
            var width = handle.width();
            var id = handle.attr("id");
            var hotid = "#" + id + " " + options.hotregion;
            $(hotid).mousedown(function (e) {
                mousedown = true;
                var e = e || window.event;
                offsetX = e.clientX;
                offsetY = e.clientY;
                $(this).css('cursor', 'move');
            });
            $(document).mouseup(function () {
                mousedown = false;
                $(hotid).css('cursor', 'default');
            }).mousemove(function (e) {
                if (!mousedown)
                    return;
                var left = parseFloat(handle.css("left"));
                var top = parseFloat(handle.css("top"));
                var e = e || window.event;
                var x = left + (e.clientX - offsetX);
                var y = top + (e.clientY - offsetY);
                offsetX = e.clientX;
                offsetY = e.clientY;
                handle.css({ "left": x + "px", "top": y + "px" });
            });
        });
        return handle;
    }
})(jQuery);
